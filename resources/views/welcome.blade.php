<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

    </head>
    <body>
    <div>
        @if(isset($planet))
            <h1>{{ $planet->name }}</h1>
            <p><strong>Description:</strong> <br>{{ $planet->description }}</p>
            <p><strong>Size:</strong> <br>{{ $planet->size_in_km }}</p>
        @else
            <h1>Planets</h1>
            <ul style="list-style: none;">
                @foreach ($planets as $planet)
                    <li>
                        <h2>{{ $planet->name }}</h2>
                        <p><strong>Description:</strong> <br>{{ $planet->description }}</p>
                        <p><strong>Size:</strong> <br>{{ $planet->size_in_km }}</p>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
    </body>
</html>
