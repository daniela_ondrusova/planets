<?php

namespace App\Http\Controllers;
use App\Models\Planet;

class PlanetController extends Controller
{
    public function index()
    {
        $planets = Planet::all();
        return view('welcome', ['planets' => $planets]);
    }

    public function show($planetName)
    {        
        // Converteer de planeetnaam naar kleine letters
        $planetName = strtolower($planetName);
        
        // Zoek de specifieke planeet op basis van de naam (in kleine letters)
        $selectedPlanet = Planet::where('name', strtolower($planetName))->first();

        return view('welcome', ['planet' => $selectedPlanet]);
    }
}
